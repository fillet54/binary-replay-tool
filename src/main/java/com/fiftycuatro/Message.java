package com.fiftycuatro;

import java.math.BigInteger;

public class Message {
    private String id;
    private long seekPos;
    private String path;
    private BigInteger timestamp;

    public Message(String id, String path, long seekPos, BigInteger timestamp){
        this.id = id;
        this.path = path;
        this.seekPos = seekPos;
        this.timestamp = timestamp;
    }

    public BigInteger getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(BigInteger timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getSeekPos() {
        return seekPos;
    }

    public void setSeekPos(long seekPos) {
        this.seekPos = seekPos;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

