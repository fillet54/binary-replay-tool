/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fiftycuatro;

import org.jruby.RubyProcess;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author sapiean
 */
public class Publisher implements Runnable {

    static boolean keepRunning = true;
    
    private BlockingQueue<Message> publishQueue;
    private CopyOnWriteArrayList<Socket> connections;

    
    static public Publisher Instantiate(final BlockingQueue<Message> pubQ, final CopyOnWriteArrayList<Socket> connectionList)  {
       Publisher retval = null;

        if(null!= pubQ && null != connectionList){
            retval = new Publisher(pubQ,connectionList);
        }

        return retval;
    }

    private Publisher(final BlockingQueue<Message> pubQ, final CopyOnWriteArrayList<Socket> connectionList){
        this.connections = connectionList;
        this.publishQueue = pubQ;

    }

    private Publisher(){
        //Nada
    }

    static public void stop(){
        keepRunning = false;
    }
    
    public void run() {
        while(keepRunning){
            try {
                Message msg = publishQueue.take();
                for(Socket client : connections) {
                    String message = "Message: " + msg.getTimestamp() + "\n";

                    try {
                        client.getOutputStream().write(message.getBytes());
                    } catch (SocketException e) {
                        connections.remove(client);
                        client.close();
                    }
                }
                System.out.println("Published Message: " + msg.getId() + " " + msg.getTimestamp().longValue());
            } catch (InterruptedException e) {
                keepRunning = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
