package com.fiftycuatro.interop.jruby;

import com.fiftycuatro.MessageTransformer;
import org.jruby.embed.LocalContextScope;
import org.jruby.embed.ScriptingContainer;
import org.jruby.runtime.builtin.IRubyObject;

import java.io.InputStream;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.util.*;

public class BinDataMessageTransformer {

    private static Map<String, String> idToLabel;
    static {
        idToLabel = new HashMap<String, String>();
        idToLabel.put("181", "One Hz Status");
        idToLabel.put("182", "One Fifth Hz Status");
        idToLabel.put("183", "Sample");
    }

    public static MessageTransformer getTransformer(String transformerPath, String transformerFile, String rootTransformer) throws URISyntaxException {
        List<String> paths = new ArrayList();
        paths.add(resourcePath("gems/bindata-2.2.0/lib")); // SUPER IMPORTANT to leave trailing slash off
        paths.add(resourcePath(transformerPath));

        final ScriptingContainer scriptingContainer = new ScriptingContainer(LocalContextScope.CONCURRENT);
        scriptingContainer.setLoadPaths(paths);
        final IRubyObject rawRuby = scriptingContainer.parse(
                getTransformationSource(transformerFile, rootTransformer)).run();

        return new MessageTransformer() {
            public Map<String, Object> convert(InputStream stream) {
                Map data = scriptingContainer.callMethod(rawRuby, "convert_from_stream", stream, Map.class);
                Map<String, Object> content = (Map)convertToJavaPrimatives(data);
                return content;
            }

            public byte[] convert(Map<String, Object> data) {
                String hexString = scriptingContainer.callMethod(rawRuby, "convert_to_bytes", data, String.class);
                return hexStringToByteArray(hexString);
            }

            public int maxSize() {
                Integer size = scriptingContainer.callMethod(rawRuby, "num_bytes", Integer.class);
                return size;
            }
        };
    }

    private static String resourcePath (String path) throws URISyntaxException {
        return ClassLoader.getSystemResource(path).toURI().toString();
    }

    private static String getTransformationSource(String require, String rootTransformer) {
        StringBuilder sb = new StringBuilder();
        sb.append("require \"java\"\n");
        sb.append("require \"" + require + "\"\n\n");
        sb.append("class BinDataMessageTransformer\n" +
                "  def initialize\n" +
                "    super\n" +
                "  end\n" +
                "\n" +
                "  def convert_from_stream(byte_stream)\n" +
                "    io = byte_stream.to_io\n" +
                "    obj = " + rootTransformer + ".read(io)\n" +
                "\n" +
                "    obj.snapshot\n" +
                "  end\n" +
                "\n" +
                "  def symbolize(obj)\n" +
                "    return obj.reduce({}) do |memo, (k, v)|\n" +
                "      memo.tap { |m| m[k.to_sym] = symbolize(v) }\n" +
                "    end if obj.is_a? Java::JavaUtil::HashMap\n" +
                "      \n" +
                "    return obj.reduce([]) do |memo, v| \n" +
                "      memo << symbolize(v); memo\n" +
                "    end if obj.is_a? Java::JavaUtil::ArrayList\n" +
                "    \n" +
                "    obj\n" +
                "  end\n" +
                "\n" +
                "  def convert_to_bytes(map_data)\n" +
                "    " + rootTransformer + ".new(symbolize(map_data)).to_hex\n" +
                "  end\n" +
                "\n" +
                "  def num_bytes()\n" +
                "    obj = " + rootTransformer + ".new\n" +
                "    obj.num_bytes\n" +
                "  end\n" +
                "end\n");
        sb.append("BinDataMessageTransformer.new\n");
        return sb.toString();
    }

    // Mostly needed due to RubySymbol
    // We want these as strings
    private static Object convertToJavaPrimatives(Object rubyObj) {
        if (rubyObj.getClass() == org.jruby.RubySymbol.class) {
            return rubyObj.toString();
        } else if (rubyObj.getClass() == org.jruby.RubyHash.class) {
            return convertToJavaMap((Map)rubyObj);
        } else if (rubyObj.getClass() == org.jruby.RubyArray.class) {
            return convertToJavaList((List) rubyObj);
        } else if (rubyObj.getClass() == java.lang.Long.class) {
            return BigInteger.valueOf((Long)rubyObj);
        } else {
            return rubyObj;
        }
    }

    private static Map<String, Object> convertToJavaMap(Map<Object, Object> rubyMap) {
        Map javaMap = new HashMap<String, Object>();
        for(Map.Entry entry : rubyMap.entrySet()) {
            javaMap.put(entry.getKey().toString(), convertToJavaPrimatives(entry.getValue()));
        }
        return javaMap;
    }

    private static List<Object> convertToJavaList(List<Object> rubyList) {
        List<Object> javaList = new ArrayList<Object>();
        for(Object obj : rubyList) {
            javaList.add(convertToJavaPrimatives(obj));
        }
        return javaList;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}