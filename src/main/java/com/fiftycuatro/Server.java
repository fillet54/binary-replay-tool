package com.fiftycuatro;


import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by sapiean on 2/23/16.
 */
public class Server implements Runnable {

    private static final int TIMEOUT = 1000;
    private static Boolean keepRunning = true;
    private ServerSocket serverSocket;
    private CopyOnWriteArrayList<Socket> clientList;
    private int listenerPort;

    static public Server Instantiate(int port, CopyOnWriteArrayList<Socket> clients){
        Server retval = null;
        if(null!= clients){
            retval = new Server(port,clients);
        }
        return retval;
    }



    private Server(int port, CopyOnWriteArrayList<Socket> clients){
        this.listenerPort = port;
        this.clientList = clients;
        serverSocket = null;
    }

    private Server(){

    }

    public void run() {

        while(keepRunning){

            try {
                if (serverSocket == null) {
                    serverSocket = new ServerSocket(this.listenerPort);
                    serverSocket.setSoTimeout(TIMEOUT);
                }

                Socket newClient = serverSocket.accept();
                clientList.add(newClient);
                System.out.println("Client Connected");

            }catch(SocketTimeoutException ste){
                //Not really an exception, continue on....
            }
            catch(Exception e){
                System.out.println(e.toString());
            }

        }


    }
}
