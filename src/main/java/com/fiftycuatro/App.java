package com.fiftycuatro;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.*;

import static java.util.Collections.singletonList;

public class App
{
    private static List<Message> loadedMessages = new ArrayList<>();

    public static void main( String[] args ) throws InterruptedException {
        int port = 52682;

        CopyOnWriteArrayList<Socket> clients = new CopyOnWriteArrayList<>();
        BlockingQueue<Message> outputQueue = new LinkedBlockingQueue<>();

        Server server = Server.Instantiate(port,clients);
        Publisher publisher = Publisher.Instantiate(outputQueue,clients);

        ExecutorService es = Executors.newFixedThreadPool(3);

        es.execute(server);
        es.execute(publisher);

        processArguments(args);

        System.out.println( "Running..." );

        Scheduler scheduler = new Scheduler(loadedMessages , outputQueue);
        scheduler.start();

        while(true);
    }

    public static void processArguments(String[] args) {
        if (args.length > 0) {
            System.out.println("Loading Messages...");
            Queue<String> inFiles = new LinkedList<>(Arrays.asList(args));
            while(!inFiles.isEmpty()) {
                String path = inFiles.poll();
                loadFile(new File(path));
            }
        }
    }

    public static void loadFile(File file) {
        try {
            if (file.isDirectory()) {
                System.out.println("Loading Directory" + file.getCanonicalPath() + "...");
                for (File innerFile : file.listFiles()) {
                    loadFile(innerFile);
                }
            } else {
                System.out.print("Loading " + file.getCanonicalPath() + "...");
                addFile(file.getCanonicalPath());
            }
        } catch (IOException e) {
            System.err.println ("Count not load file. " + e.getMessage());
        }
        System.out.println("Done");
    }
    public static void addFile(String path) {
        List<Message> messages = FileProcessor.Process(singletonList(new File(path)));

        for(Message message : messages) {
            loadedMessages .add(message);
        }
    }
}
