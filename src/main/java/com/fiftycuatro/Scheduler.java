package com.fiftycuatro;

import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.BlockingQueue;

public class Scheduler implements Runnable {

    private List<Message> sortedMessages;
    private BlockingQueue<Message> outputQueue;

    public Scheduler(Collection<Message> messages, BlockingQueue<Message> outputQueue) {
        this.sortedMessages = sortByTimestamp(new ArrayList<>(messages));
        this.outputQueue = outputQueue;
    }

    private Thread runThread;
    public void start() {
        runThread = new Thread(this);
        runThread.setName("Scheduler");
        runThread.start();
    }

    public void stop() {
        if (runThread != null) {
            runThread.interrupt();
            runThread = null;
        }
    }

    public void run() {
        BigInteger actualStartTime = BigInteger.valueOf(new Date().getTime());
        BigInteger referenceTime = sortedMessages.get(0).getTimestamp();

        for(Message msg : sortedMessages) {
            BigInteger actualDelta = BigInteger.valueOf(new Date().getTime()).subtract(actualStartTime);
            BigInteger referenceDelta = msg.getTimestamp().subtract(referenceTime);

            if (referenceDelta.compareTo(actualDelta) > 0) {
                try {
                    Thread.sleep(referenceDelta.subtract(actualDelta).longValueExact());
                } catch (InterruptedException e) {
                   return;
                }
            }
            outputQueue.offer(msg);

            if (Thread.interrupted()) {
                return;
            }
        }
    }

    public List<Message> sortByTimestamp(List<Message> messages) {
        Collections.sort(messages, (c1, c2) -> c1.getTimestamp().compareTo(c2.getTimestamp()));
        return messages;
    }
}
