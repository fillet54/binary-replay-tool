package com.fiftycuatro;

import com.fiftycuatro.interop.jruby.BinDataMessageTransformer;

import java.io.*;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by sapiean on 2/28/16.
 *     foreach file in filelist
 *          parse binary into individual messages
 *          get utc time of message
 *          get message id/type
 *          add additional headers to       //Do this somewhere else?
 *          add msg to retval
 */


/**
 * Algorithm #2 - redesign
 *
 * Multi-pass
 *  pass 1
 *      Create index filename tuple for each message using StreamSearcher
 *  pass 2
 *      for number of messages we which to process, or some other limit, get the messages...
 *      we could do something smart, like order the files based on time of the first of each of the messages,
 */
public class FileProcessor {

    private static final byte[] SYNC_PATTERN = {(byte)0xff,(byte)0x00,(byte)0xff,(byte)0x00};      //TODO:  Make this configurable?

    private static StreamSearcher searcher;

    static List<Message> Process(final List<File> files){

        List<Message> retval = new ArrayList<Message>();
        searcher = new StreamSearcher(FileProcessor.SYNC_PATTERN);

        MessageTransformer transformer;

        try {
            transformer =
                    new BinDataMessageTransformer()
                            .getTransformer(
                                    "com/fiftycuatro/ruby",
                                    "message_struct",
                                    "P2Header");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }

        for (File f : files){
            List<Message> fileMessages = new ArrayList<>();
            try {
                FileInputStream fis = new FileInputStream(f);

                long bytesconsumed = searcher.search(fis);
                long index = bytesconsumed - SYNC_PATTERN.length;
                while(bytesconsumed > -1){
                    fileMessages.add(
                            new Message(
                                    f.getCanonicalPath() + "@" + index,
                                    f.getCanonicalPath(),
                                    index,
                                    BigInteger.ZERO));
                    bytesconsumed = searcher.search(fis);       //TODO: I know there is a better way to do this
                    index += bytesconsumed;
                }

                int count = 0;
                // Get Timestamps
                for(Message message : fileMessages) {
                    count++;
                    fis.getChannel().position(message.getSeekPos());
                    byte[] header_bytes = new byte[transformer.maxSize()];
                    int read = fis.read(header_bytes);
                    if (read <= 0) {
                        System.out.println("ERROR on " + count);
                        continue;
                    }
                    InputStream in = new ByteArrayInputStream(header_bytes);
                    Map<String, Object> header = transformer.convert(in);

                    BigInteger timestamp = (BigInteger) header.get("timestamp");
                    message.setTimestamp(timestamp);
                }

                fis.close();
            }catch(FileNotFoundException e){
                System.err.println(e.getMessage());     //TODO: Logger
            }catch(IOException e){
                System.err.println(e.getMessage());     //TODO: Logger
            }

            retval.addAll(fileMessages);
        }

        return retval;
    }
}
