package com.fiftycuatro;

import java.io.InputStream;
import java.util.Map;

public interface MessageTransformer {
    public Map<String, Object> convert(InputStream stream);
    public byte[] convert(Map<String, Object> data);
    public int maxSize();
}
