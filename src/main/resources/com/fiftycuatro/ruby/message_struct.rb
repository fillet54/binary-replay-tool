require "bindata"

class P2Header < BinData::Record
    endian :big
    uint8 :sync1, :value => 0xff
    uint8 :sync2, :value => 0x00
    uint8 :sync3, :value => 0xff
    uint8 :sync4, :value => 0x00
    uint64 :timestamp
    uint16 :msgid
end