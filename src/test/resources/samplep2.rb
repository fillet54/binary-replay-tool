require 'bindata'
require 'time'

class Samplep2 < BinData::Record
    endian :big
    uint8 :sync1, :value => 0xff
    uint8 :sync2, :value => 0x00
    uint8 :sync3, :value => 0xff
    uint8 :sync4, :value => 0x00
    uint64 :timestamp
    uint16 :msgid
end


def generate_file(filename,numMsgs, startTime)
    currentTime = startTime
    File.open(filename,"wb") do |io|
        numMsgs.times { |i|
            msg = Samplep2.new(:timestamp => currentTime, :msgid => 26)
            msg.write(io)
            currentTime += rand(1000)
            puts msg.timestamp
        }
    end
    currentTime
end

if $0 == __FILE__
   time = generate_file("one.p2", 50, 0)
   time = generate_file("two.p2", 100, time)
   time = generate_file("three.p2", 150, time)
end


                
