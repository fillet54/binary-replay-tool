package com.fiftycuatro;

import com.fiftycuatro.interop.jruby.BinDataMessageTransformer;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

public class AppTest
{
    @Test
    public void canGetMaxSizeForP2Header() throws IOException, URISyntaxException {
        MessageTransformer transformer =
                new BinDataMessageTransformer()
                        .getTransformer(
                                "com/fiftycuatro/ruby",
                                "message_struct",
                                "P2Header");

        assertEquals(14, transformer.maxSize());
    }
}
