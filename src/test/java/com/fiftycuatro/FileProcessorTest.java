package com.fiftycuatro;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sapiean on 2/28/16.
 */
public class FileProcessorTest {

    @Test
    public void TestProcessing(){

        //Files.find(FileSystems.getDefault().getPath(),1,(fPath, fAttrib) -> {  fPath.endsWith(".p2"); },FileVisitOption.FOLLOW_LINKS);

        ClassLoader cl = getClass().getClassLoader();
        List<File> files = new ArrayList<File>();
        files.add(new File(cl.getResource("one.p2").getFile()));
        files.add(new File(cl.getResource("two.p2").getFile()));
        files.add(new File(cl.getResource("three.p2").getFile()));

        List<Message> msgs = FileProcessor.Process(files);

        System.out.println(msgs.size());

        for (Message m : msgs) {
            System.out.println(m.getPath() + " " + m.getSeekPos() + " " + m.getTimestamp() );
        }
    }
}
